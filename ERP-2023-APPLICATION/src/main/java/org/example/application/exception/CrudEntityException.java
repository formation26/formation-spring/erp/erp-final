package org.example.application.exception;

public class CrudEntityException extends Exception{

    public CrudEntityException(String message) {
        super(message);
    }
}
