package org.example.configuration;

import org.example.domaine.Produit;
import org.example.dto.ProduitDetailsDto;
import org.springframework.batch.core.*;
import org.springframework.batch.item.ItemReader;
import org.springframework.stereotype.Component;

import java.io.File;
@Component
public class StepListenerERP implements StepExecutionListener {
    @Override
    public void beforeStep(StepExecution stepExecution) {
        File file=new File("produits.csv");
        file.setReadOnly();
        System.out.println("Le fichier est libre");
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        File fileJson=new File("produits.json");
        System.out.println("Vérification du fichier JSON");

        File fileCSV=new File("produits.csv");
        fileCSV.setWritable(true);

        if(fileJson.exists() && fileJson.getTotalSpace()>0){
            System.out.println("Le fichier existe");
            return ExitStatus.COMPLETED;
        }else{
            return ExitStatus.FAILED;
        }
    }
}
